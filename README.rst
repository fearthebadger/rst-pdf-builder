.. README.rst:

README for the rst_pdf_builder
==============================

This is a tool to help you build you documents. It is based on the
`Sphinx Document Builder <http://www.sphinx-doc.org/en/master/>`_.

These instructions should help you get through the entire install process.

Requirements
------------

* ``docker`` installed and you have access to it.
* Copy your restructured text to the source directory.

Build the document_builder container
-------------------------------------------

#. Build container

   .. code-block:: bash

      docker build -t document_builder:latest .


Build script
------------

.. code-block:: bash

  ./build_docs

`build_docs` will do everything needed to build the document.

   #. Runs the docker command to build the document.
   #. Copies the resulting pdf to the current directory.
   #. Cleans up everything after it is complete.

ToDo
----

* Pass the user on to the container so that removal doesn't need to be done in
  the container.
* Add a flag to prevent cleanup if needed.
* Move the _build directory out of source
