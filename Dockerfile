# Foundation: ubuntu
FROM python:3.8.1-slim-buster
MAINTAINER Brock R.

# Use the docs directory to build you document
WORKDIR /docs

# Set it so that the install is nointeractive so that things like
# timezone can just pick the default.
ENV DEBIAN_FRONTEND noninteractive
ENV PATH="${PATH}:/usr/local/texlive/current/bin/x86_64-linux"

# Mount the notes dir to the docs dir inside the container
VOLUME .:/docs

RUN echo "Building" && \
  \
  # Update the ubuntu apt repo sources
  apt-get update -q && \
  \
  # Install the packages to build TexLive
  apt-get install -qy build-essential wget libfontconfig1 && \
  \
  # Install Python and Sphinx
  apt-get install -qy python-sphinx && \
  \
  # Download the TexLive installer
  wget http://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz && \
  mkdir /install-tl-unx && \
  tar -xvf install-tl-unx.tar.gz -C /install-tl-unx --strip-components=1 && \
  \
  # Setup the profile so that TexLive only installs the Basic Scheme
  echo "selected_scheme scheme-basic" >> /install-tl-unx/texlive.profile && \
  \
  # Install TexLive
  /install-tl-unx/install-tl -profile /install-tl-unx/texlive.profile && \
  \
  # Link the installed directory to a current so that it can be refenced by $PATH
  ln -s /usr/local/texlive/20* /usr/local/texlive/current && \
  \
  # Install only the latex packages I needed
  tlmgr install latexmk \
                cmap \
                collection-fontsrecommended \
                fncychap \
                titlesec \
                tabulary \
                varwidth \
                framed \
                xcolor \
                fancyvrb \
                float \
                parskip \
                upquote \
                capt-of \
                needspace \
                pgf \
                wrapfig \
                etoolbox \
                background \
                xkeyval \
                everypage \
                tikzpagenodes \
                ifoddpage \
                pdfescape \
                letltxmacro \
                bitset \
                && \
  \
  # Remove all the things that were downloaded and are no longer needed.
  rm -r /install-tl-unx && \
  rm install-tl-unx.tar.gz && \
  rm -rf /var/lib/apt/lists/* && \
  rm -rf /var/cache/*
